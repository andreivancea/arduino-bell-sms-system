int getRelayLevel(char state) {
  if (state == 'x') {
    return HIGH;
  } else {
    return LOW;
  }
}

void setRelays(int pinState) {
//    Serial.println("Set relays...");
  for (int i = 0; i < PIN_MASK_SIZE; i++) {
    relayMask[i] = pinState;
    digitalWrite(pinMask[i], relayMask[i]);
  }
//    Serial.println("Relays setted");
}

void actionRelays() {
  hasRelaysOn = true;
  for (int i = 0; i < PIN_MASK_SIZE; i++) {
    digitalWrite(pinMask[i], relayMask[i]);
//          Serial.print("Pin ");
//          Serial.print(pinMask[i]);
//          Serial.print(" set to:");
//          Serial.println(relayMask[i]);
  }
  //  Serial.print("Waiting for ");
  //  Serial.print(minutes);
  //  Serial.println(" minutes");

  //  delay(minutes * oneMinuteInMs);
  //  Serial.println("Done waiting!");
  delay(1000);
  timeElapsed = 0;
}
