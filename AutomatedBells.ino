
// Serial Message Template
// $xxccx2m - suggestion $xxccx#2m - for reading variable pin number states
#include <elapsedMillis.h>
#include <SoftwareSerial.h>
SoftwareSerial mySerial(2, 3);

#define PIN_MASK_SIZE 4
#define MAX_NUMBER_OF_MINUTES 5

elapsedMillis timeElapsed; //declare global if you don't want it reset every time loop runs
elapsedMillis messageExpiryTimeElapsed;

String message;
bool isReadingMsg;
bool hasRelaysOn;
int pinMask[] = {4, 5, 6, 7};
int minutesTillOver;
int relayMask[25];
unsigned long oneMinuteInMs = 60000L;
boolean shouldDeleteMessages = false;

void prepareSerial()
{
  for (int i = 0; i < PIN_MASK_SIZE; i++) {
    pinMode(pinMask[i], OUTPUT);
  }
  setRelays(HIGH);

  // console debug
  delay(10000);
  Serial.begin(9600);

  // init serial
  mySerial.begin(57600);
  delay(5000); 
  Serial.println("Waiting for SIM 900 configuration...");
  delay(1000);
  // setup sim comm
  mySerial.write("AT\r");
  delay(1000);
  mySerial.write("AT+CPMS=\"SM\",\"SM\",\"SM\"\r");
  delay(1000);
  mySerial.write("AT+CSQ\r");
  delay(1000);
  mySerial.write("AT+CMEE=2\r");
  delay(1000);
  mySerial.write("AT+CMGD=4,4\r");
  delay(1000);
  // prepare for txt message
  mySerial.write("AT+CMGF=1\r"); //set message reading as text
  delay(1000);

  resetMessage();
};

void setup() {
  prepareSerial();
}

void loop() {
  if (mySerial.available()) {
    char charRead = mySerial.read();
    parseMessage(charRead);
  }
  if (hasRelaysOn && timeElapsed > minutesTillOver * oneMinuteInMs)
  {
    hasRelaysOn = false;
    setRelays(HIGH);
  }
  if (shouldDeleteMessages && messageExpiryTimeElapsed > 1000){
    shouldDeleteMessages = false;
    deleteAllMessages();
  }
}
String smsFull = "S";
boolean isSmsReading;
void parseMessage(char charRead) {
  Serial.write(charRead);
  if (isReadingMsg) {
    message += charRead;
    if (charRead == 'm') { //this must be the last char from the message
//      Serial.println(message);
      deserializeMessage(message);
      isReadingMsg = false;
      delay(1000);
      resetMessage();
    }
  }
  if(isSmsReading){
    smsFull += charRead;
    if(smsFull.length() > 4){
      smsFull = "+";
      isSmsReading = false;
    }
    if(smsFull.equals("+CMT")){
      shouldDeleteMessages = true;
      isSmsReading = false;
      smsFull = "+";
    }
  }
  if (charRead == '$') {
    message = "";
    isReadingMsg = true;
  }
  
  if (charRead == '+'){
    messageExpiryTimeElapsed = 0;
    isSmsReading = true;  
  }
}
