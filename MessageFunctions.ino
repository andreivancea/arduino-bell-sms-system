String minutesAsString;
int minutes;
//c1cxx1m
void deserializeMessage(String message) {
  //check if message size is good
  if (message.length() < 2) {
    return;
  }
//  Serial.println("Length Check: OK!");
  //set relay mask
  for (int i = 0; i < message.length() - 2; i++) {
    if (isRelayChar(message[i])) {
      relayMask[i] = getRelayLevel(message[i]);
    }
  }

  char minuteChar = message[message.length() - 2];
//  Serial.print("MINUTE CHAR -> ");
//  Serial.println(minuteChar);
  if (isDigit(minuteChar)) {
//    Serial.println("CHAR IS DIGIT");
    String mins = "";
    mins += (char)minuteChar;
    minutes = mins.toInt();
  } else {
    return;
  }

//  Serial.print("MINUTE CHECK: OK ->");
//  Serial.println(minutes);
  
  if (minutes > MAX_NUMBER_OF_MINUTES) {
    minutes = MAX_NUMBER_OF_MINUTES;
  }

  minutesTillOver = minutes;

  actionRelays();
}

void resetMessage() {
  message = "";
  isReadingMsg = false;

  mySerial.write("AT+CMGD=4,4\r");
  delay(1000);

  // wait for next message
  mySerial.write("AT+CNMI=2,2,0,0,0\r");
  delay(1000);
}

void deleteAllMessages(){
  mySerial.write("AT+CMGD=4,4\r");
}
